<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-07-31 16:49:10 by shbd-->
<display version="2.0.0">
  <name>$(P)$(R)</name>
  <width>1540</width>
  <height>1100</height>
  <background_color>
    <color red="220" green="220" blue="220">
    </color>
  </background_color>
  <actions>
  </actions>
  <widget type="group" version="3.0.0">
    <name>H1 group</name>
    <y>50</y>
    <width>710</width>
    <height>840</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>BGGrey03-background</name>
      <width>710</width>
      <height>840</height>
      <line_width>0</line_width>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>5</corner_width>
      <corner_height>5</corner_height>
    </widget>
    <widget type="group" version="3.0.0">
      <name>H1 Current</name>
      <x>19</x>
      <y>50</y>
      <width>290</width>
      <height>110</height>
      <style>3</style>
      <widget type="label" version="2.0.0">
        <name>LGGrey02-title_4</name>
        <text>Output Control</text>
        <width>290</width>
        <height>40</height>
        <font>
          <font family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
          </font>
        </font>
        <foreground_color>
          <color name="GROUP-TEXT" red="25" green="25" blue="25">
          </color>
        </foreground_color>
        <background_color>
          <color name="RED-BORDER" red="150" green="8" blue="16">
          </color>
        </background_color>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <wrap_words>false</wrap_words>
      </widget>
      <widget type="label" version="2.0.0">
        <name>X Set Position Caption_4</name>
        <text>Cur. Setpoint:</text>
        <x>10</x>
        <y>40</y>
        <horizontal_alignment>2</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="spinner" version="2.0.0">
        <name>Spinner Template_20</name>
        <pv_name>${P}${R}Cur-S</pv_name>
        <x>120</x>
        <y>40</y>
        <format>0</format>
        <precision>5</precision>
        <actions>
        </actions>
        <rules>
          <rule name="RB Match" prop_id="background_color" out_exp="false">
            <exp bool_exp="abs(pv0-pv1)&gt;1">
              <value>
                <color name="ATTENTION" red="252" green="242" blue="17">
                </color>
              </value>
            </exp>
            <pv_name>${P}${R}Cur-S</pv_name>
            <pv_name>${P}${R}Cur-R</pv_name>
          </rule>
        </rules>
        <border_alarm_sensitive>false</border_alarm_sensitive>
        <minimum>-20.0</minimum>
        <maximum>20.0</maximum>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update Template_19</name>
        <pv_name>${P}${R}Cur-R</pv_name>
        <x>120</x>
        <y>70</y>
        <foreground_color>
          <color red="0" green="0" blue="0">
          </color>
        </foreground_color>
        <precision>5</precision>
        <actions>
        </actions>
      </widget>
      <widget type="label" version="2.0.0">
        <name>X Set Position Caption_5</name>
        <text>Cur. Readback:</text>
        <x>2</x>
        <y>70</y>
        <width>110</width>
        <horizontal_alignment>2</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="action_button" version="3.0.0">
        <name>Action Button</name>
        <actions>
          <action type="write_pv">
            <pv_name>$(pv_name)</pv_name>
            <value>1</value>
            <description>WritePV</description>
          </action>
        </actions>
        <pv_name>${P}${R}Cur-S.PROC</pv_name>
        <text>&gt;&gt;</text>
        <x>235</x>
        <y>40</y>
        <width>40</width>
        <height>20</height>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
    </widget>
    <widget type="group" version="3.0.0">
      <name>H1 Local</name>
      <x>19</x>
      <y>371</y>
      <width>290</width>
      <height>75</height>
      <style>3</style>
      <widget type="led" version="2.0.0">
        <name>Text Update_11</name>
        <pv_name>$(P)$(R)CtrlMode</pv_name>
        <bit>0</bit>
        <x>163</x>
        <y>35</y>
        <width>100</width>
        <height>30</height>
        <off_label>REMOTE</off_label>
        <off_color>
          <color name="OK" red="61" green="216" blue="61">
          </color>
        </off_color>
        <on_label>LOCAL</on_label>
        <on_color>
          <color name="LED-ORANGE-ON" red="255" green="175" blue="81">
          </color>
        </on_color>
        <square>true</square>
        <labels_from_pv>true</labels_from_pv>
      </widget>
      <widget type="led" version="2.0.0">
        <name>Text Update_13</name>
        <pv_name>$(P)$(R)RegMode</pv_name>
        <bit>0</bit>
        <x>20</x>
        <y>35</y>
        <width>100</width>
        <height>30</height>
        <off_label>CURRENT</off_label>
        <off_color>
          <color name="OK" red="61" green="216" blue="61">
          </color>
        </off_color>
        <on_label>VOLTAGE</on_label>
        <on_color>
          <color name="LED-ORANGE-ON" red="255" green="175" blue="81">
          </color>
        </on_color>
        <square>true</square>
        <labels_from_pv>true</labels_from_pv>
      </widget>
      <widget type="label" version="2.0.0">
        <name>X Actual Position Caption_3</name>
        <text>Mode (I/V)</text>
        <x>20</x>
        <y>10</y>
        <width>69</width>
        <height>25</height>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <auto_size>true</auto_size>
      </widget>
      <widget type="label" version="2.0.0">
        <name>X Actual Position Caption_4</name>
        <text>Local/Remote</text>
        <x>163</x>
        <y>10</y>
        <width>94</width>
        <height>25</height>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <auto_size>true</auto_size>
      </widget>
    </widget>
    <widget type="group" version="3.0.0">
      <name>H1 Errors</name>
      <x>19</x>
      <y>286</y>
      <width>290</width>
      <height>75</height>
      <style>3</style>
      <widget type="action_button" version="3.0.0">
        <name>Action Button Template_11</name>
        <actions>
          <action type="write_pv">
            <pv_name>$(pv_name)</pv_name>
            <value>1</value>
            <description>WritePV</description>
          </action>
        </actions>
        <pv_name>${P}${R}Rst</pv_name>
        <text>Reset</text>
        <x>163</x>
        <y>35</y>
        <width>102</width>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
      <widget type="led" version="2.0.0">
        <name>Text Update_12</name>
        <pv_name>${P}${R}Fault-R</pv_name>
        <bit>0</bit>
        <x>20</x>
        <y>35</y>
        <width>100</width>
        <height>30</height>
        <off_label>OK</off_label>
        <off_color>
          <color name="OK" red="61" green="216" blue="61">
          </color>
        </off_color>
        <on_label>NOK</on_label>
        <on_color>
          <color name="ERROR" red="252" green="13" blue="27">
          </color>
        </on_color>
        <square>true</square>
      </widget>
      <widget type="label" version="2.0.0">
        <name>X Actual Position Caption_1</name>
        <text>Fault Status</text>
        <x>20</x>
        <y>10</y>
        <width>81</width>
        <height>25</height>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <auto_size>true</auto_size>
      </widget>
      <widget type="label" version="2.0.0">
        <name>X Actual Position Caption_2</name>
        <text>Reset Faults</text>
        <x>163</x>
        <y>10</y>
        <width>82</width>
        <height>25</height>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <auto_size>true</auto_size>
      </widget>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update Template_21</name>
      <pv_name>${P}${R}Model-R</pv_name>
      <x>19</x>
      <width>666</width>
      <height>40</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <foreground_color>
        <color name="GROUP-TEXT" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <precision>2</precision>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <actions>
      </actions>
    </widget>
    <widget type="group" version="3.0.0">
      <name>H1 Voltage</name>
      <x>19</x>
      <y>176</y>
      <width>290</width>
      <height>100</height>
      <style>3</style>
      <widget type="label" version="2.0.0">
        <name>LGGrey02-title_3</name>
        <text>Output Feedback</text>
        <width>290</width>
        <height>40</height>
        <font>
          <font family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
          </font>
        </font>
        <foreground_color>
          <color name="GROUP-TEXT" red="25" green="25" blue="25">
          </color>
        </foreground_color>
        <background_color>
          <color name="RED-BORDER" red="150" green="8" blue="16">
          </color>
        </background_color>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <wrap_words>false</wrap_words>
      </widget>
      <widget type="label" version="2.0.0">
        <name>X Actual Position Caption</name>
        <class>CAPTION</class>
        <text>Power:</text>
        <x>52</x>
        <y>70</y>
        <width>71</width>
        <height>25</height>
        <foreground_color use_class="true">
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </foreground_color>
        <horizontal_alignment use_class="true">2</horizontal_alignment>
        <vertical_alignment use_class="true">1</vertical_alignment>
      </widget>
      <widget type="label" version="2.0.0">
        <name>X Set Position Caption_3</name>
        <class>CAPTION</class>
        <text>Voltage:</text>
        <x>7</x>
        <y>40</y>
        <width>116</width>
        <foreground_color use_class="true">
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </foreground_color>
        <horizontal_alignment use_class="true">2</horizontal_alignment>
        <vertical_alignment use_class="true">1</vertical_alignment>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update Template_18</name>
        <pv_name>${P}${R}OutPwr-R</pv_name>
        <x>133</x>
        <y>70</y>
        <width>111</width>
        <foreground_color>
          <color red="0" green="0" blue="0">
          </color>
        </foreground_color>
        <precision>5</precision>
        <actions>
        </actions>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update Template_20</name>
        <pv_name>${P}${R}Vol-R</pv_name>
        <x>133</x>
        <y>40</y>
        <width>111</width>
        <foreground_color>
          <color red="0" green="0" blue="0">
          </color>
        </foreground_color>
        <precision>5</precision>
        <actions>
        </actions>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
    </widget>
    <widget type="group" version="3.0.0">
      <name>Group</name>
      <x>360</x>
      <y>50</y>
      <width>315</width>
      <height>206</height>
      <style>3</style>
      <transparent>true</transparent>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update</name>
        <pv_name>$(P)$(R)FWVer-R</pv_name>
        <x>105</x>
        <y>25</y>
        <width>210</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_1</name>
        <text>FW. Version</text>
        <y>25</y>
        <width>90</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <horizontal_alignment>2</horizontal_alignment>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update_1</name>
        <pv_name>$(P)$(R)Model-R</pv_name>
        <x>105</x>
        <width>210</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_2</name>
        <text>Model</text>
        <width>90</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <horizontal_alignment>2</horizontal_alignment>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update_2</name>
        <pv_name>$(P)$(R)ErrorMsg-R</pv_name>
        <x>105</x>
        <y>50</y>
        <width>210</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_3</name>
        <text>Last Error Msg</text>
        <y>50</y>
        <width>90</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <horizontal_alignment>2</horizontal_alignment>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update_4</name>
        <pv_name>$(P)$(R)DCLnkVol-R</pv_name>
        <x>105</x>
        <y>100</y>
        <width>210</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_4</name>
        <text>DC Link Vol.</text>
        <y>100</y>
        <width>90</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <horizontal_alignment>2</horizontal_alignment>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update_5</name>
        <pv_name>$(P)$(R)GndCur-R</pv_name>
        <x>105</x>
        <y>75</y>
        <width>210</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_5</name>
        <text>Ground Cur.</text>
        <y>75</y>
        <width>90</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <horizontal_alignment>2</horizontal_alignment>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update_6</name>
        <pv_name>$(P)$(R)HeatTmp-R</pv_name>
        <x>105</x>
        <y>126</y>
        <width>210</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_6</name>
        <text>Heatsink Temp.</text>
        <y>126</y>
        <width>90</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <horizontal_alignment>2</horizontal_alignment>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update_7</name>
        <pv_name>$(P)$(R)SlewRateI-R</pv_name>
        <x>105</x>
        <y>160</y>
        <width>210</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <precision>5</precision>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_7</name>
        <text>Cur. Slew Rate</text>
        <y>160</y>
        <width>90</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <horizontal_alignment>2</horizontal_alignment>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update_8</name>
        <pv_name>$(P)$(R)SlewRateV-R</pv_name>
        <x>105</x>
        <y>186</y>
        <width>210</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <precision>5</precision>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_18</name>
        <text>Vol. Slew Rate</text>
        <y>186</y>
        <width>90</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <horizontal_alignment>2</horizontal_alignment>
      </widget>
    </widget>
    <widget type="group" version="3.0.0">
      <name>Group</name>
      <x>19</x>
      <y>456</y>
      <width>290</width>
      <height>46</height>
      <style>3</style>
      <transparent>true</transparent>
      <widget type="label" version="2.0.0">
        <name>LGGrey02-title_5</name>
        <text>ON/OFF Command</text>
        <y>10</y>
        <width>160</width>
        <height>30</height>
        <font>
          <font family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
          </font>
        </font>
        <foreground_color>
          <color name="GROUP-TEXT" red="25" green="25" blue="25">
          </color>
        </foreground_color>
        <background_color>
          <color name="RED-BORDER" red="150" green="8" blue="16">
          </color>
        </background_color>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <wrap_words>false</wrap_words>
      </widget>
      <widget type="bool_button" version="2.0.0">
        <name>Boolean Button Template_28</name>
        <pv_name>${P}${R}Pwr-S</pv_name>
        <x>160</x>
        <y>10</y>
        <off_label>OFF</off_label>
        <off_color>
          <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
          </color>
        </off_color>
        <on_label>ON</on_label>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
        <actions>
        </actions>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
      <widget type="led" version="2.0.0">
        <name>Text Update_3</name>
        <pv_name>${P}${R}Status-R</pv_name>
        <bit>0</bit>
        <x>260</x>
        <y>10</y>
        <width>30</width>
        <height>30</height>
        <square>true</square>
      </widget>
    </widget>
    <widget type="group" version="3.0.0">
      <name>H1 Over Voltage Protection</name>
      <x>360</x>
      <y>270</y>
      <width>315</width>
      <height>230</height>
      <style>3</style>
      <widget type="embedded" version="2.0.0">
        <name>Embedded Display</name>
        <file>$(FAULTS_OPI)</file>
        <x>10</x>
        <width>252</width>
        <height>192</height>
        <resize>2</resize>
      </widget>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>Picture_1</name>
      <file>images/$(PS_IMAGE)</file>
      <x>19</x>
      <y>522</y>
      <width>330</width>
      <height>210</height>
    </widget>
    <widget type="group" version="3.0.0">
      <name>Asyn</name>
      <x>360</x>
      <y>519</y>
      <width>325</width>
      <height>210</height>
      <style>3</style>
      <widget type="label" version="2.0.0">
        <name>text #117</name>
        <text>Connected</text>
        <x>190</x>
        <y>60</y>
        <width>114</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <foreground_color>
          <color red="60" green="180" blue="32">
          </color>
        </foreground_color>
        <background_color>
          <color name="Button_Background" red="236" green="236" blue="236">
          </color>
        </background_color>
        <transparent>false</transparent>
        <rules>
          <rule name="vis_if_not_zero" prop_id="visible" out_exp="false">
            <exp bool_exp="!(pv0!=0)">
              <value>false</value>
            </exp>
            <pv_name>$(P)$(R)AsynRecord.CNCT</pv_name>
          </rule>
        </rules>
      </widget>
      <widget type="label" version="2.0.0">
        <name>text #121</name>
        <text>Disconnected</text>
        <x>190</x>
        <y>60</y>
        <width>114</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <foreground_color>
          <color red="253" green="0" blue="0">
          </color>
        </foreground_color>
        <background_color>
          <color name="Button_Background" red="236" green="236" blue="236">
          </color>
        </background_color>
        <transparent>false</transparent>
        <rules>
          <rule name="vis_if_zero" prop_id="visible" out_exp="false">
            <exp bool_exp="!(pv0==0)">
              <value>false</value>
            </exp>
            <pv_name>$(P)$(R)AsynRecord.CNCT</pv_name>
          </rule>
        </rules>
      </widget>
      <widget type="combo" version="2.0.0">
        <name>menu #125</name>
        <pv_name>$(P)$(R)AsynRecord.CNCT</pv_name>
        <x>60</x>
        <y>60</y>
        <width>120</width>
        <height>20</height>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
      <widget type="label" version="2.0.0">
        <name>text #22</name>
        <text>noAutoConnect</text>
        <x>190</x>
        <y>142</y>
        <width>114</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <foreground_color>
          <color red="251" green="243" blue="74">
          </color>
        </foreground_color>
        <background_color>
          <color name="Button_Background" red="236" green="236" blue="236">
          </color>
        </background_color>
        <transparent>false</transparent>
        <rules>
          <rule name="vis_if_zero" prop_id="visible" out_exp="false">
            <exp bool_exp="!(pv0==0)">
              <value>false</value>
            </exp>
            <pv_name>$(P)$(R)AsynRecord.AUCT</pv_name>
          </rule>
        </rules>
      </widget>
      <widget type="label" version="2.0.0">
        <name>text #26</name>
        <text>autoConnect</text>
        <x>190</x>
        <y>142</y>
        <width>114</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <foreground_color>
          <color red="60" green="180" blue="32">
          </color>
        </foreground_color>
        <background_color>
          <color name="Button_Background" red="236" green="236" blue="236">
          </color>
        </background_color>
        <transparent>false</transparent>
        <rules>
          <rule name="vis_if_not_zero" prop_id="visible" out_exp="false">
            <exp bool_exp="!(pv0!=0)">
              <value>false</value>
            </exp>
            <pv_name>$(P)$(R)AsynRecord.AUCT</pv_name>
          </rule>
        </rules>
      </widget>
      <widget type="combo" version="2.0.0">
        <name>menu #30</name>
        <pv_name>$(P)$(R)AsynRecord.AUCT</pv_name>
        <x>60</x>
        <y>142</y>
        <width>120</width>
        <height>20</height>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
      <widget type="label" version="2.0.0">
        <name>text #33</name>
        <text>Enabled</text>
        <x>190</x>
        <y>100</y>
        <width>114</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <foreground_color>
          <color red="60" green="180" blue="32">
          </color>
        </foreground_color>
        <background_color>
          <color name="Button_Background" red="236" green="236" blue="236">
          </color>
        </background_color>
        <transparent>false</transparent>
        <rules>
          <rule name="vis_if_not_zero" prop_id="visible" out_exp="false">
            <exp bool_exp="!(pv0!=0)">
              <value>false</value>
            </exp>
            <pv_name>$(P)$(R)AsynRecord.ENBL</pv_name>
          </rule>
        </rules>
      </widget>
      <widget type="label" version="2.0.0">
        <name>text #37</name>
        <text>Disabled</text>
        <x>190</x>
        <y>100</y>
        <width>114</width>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <foreground_color>
          <color red="253" green="0" blue="0">
          </color>
        </foreground_color>
        <background_color>
          <color name="Button_Background" red="236" green="236" blue="236">
          </color>
        </background_color>
        <transparent>false</transparent>
        <rules>
          <rule name="vis_if_zero" prop_id="visible" out_exp="false">
            <exp bool_exp="!(pv0==0)">
              <value>false</value>
            </exp>
            <pv_name>$(P)$(R)AsynRecord.ENBL</pv_name>
          </rule>
        </rules>
      </widget>
      <widget type="combo" version="2.0.0">
        <name>menu #41</name>
        <pv_name>$(P)$(R)AsynRecord.ENBL</pv_name>
        <x>60</x>
        <y>100</y>
        <width>120</width>
        <height>20</height>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LGGrey02-title_6</name>
        <text>Remote Connection Management</text>
        <width>309</width>
        <height>40</height>
        <font>
          <font family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
          </font>
        </font>
        <foreground_color>
          <color name="GROUP-TEXT" red="25" green="25" blue="25">
          </color>
        </foreground_color>
        <background_color>
          <color name="RED-BORDER" red="150" green="8" blue="16">
          </color>
        </background_color>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <wrap_words>false</wrap_words>
      </widget>
      <widget type="action_button" version="3.0.0">
        <name>Action Button_1</name>
        <actions>
          <action type="open_display">
            <file>asynRecord.bob</file>
            <macros>
              <P>$(P)$(R)</P>
              <R>AsynRecord</R>
            </macros>
            <target>window</target>
            <description>Open Display</description>
          </action>
        </actions>
        <text>asyn</text>
        <x>220</x>
        <y>180</y>
        <width>70</width>
        <height>20</height>
        <font>
          <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
          </font>
        </font>
        <tooltip>$(actions)</tooltip>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_8</name>
        <text>Conn. 
Status</text>
        <x>15</x>
        <y>50</y>
        <width>33</width>
        <height>38</height>
        <font>
          <font family="Source Sans Pro" style="REGULAR" size="12.0">
          </font>
        </font>
        <auto_size>true</auto_size>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_9</name>
        <text>Ena./Dis.</text>
        <x>10</x>
        <y>100</y>
        <width>45</width>
        <height>18</height>
        <font>
          <font family="Source Sans Pro" style="REGULAR" size="12.0">
          </font>
        </font>
        <auto_size>true</auto_size>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Label_10</name>
        <text>Auto 
Conn.</text>
        <x>15</x>
        <y>130</y>
        <width>30</width>
        <height>38</height>
        <font>
          <font family="Source Sans Pro" style="REGULAR" size="12.0">
          </font>
        </font>
        <auto_size>true</auto_size>
      </widget>
    </widget>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>710</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>TITLE</class>
    <text>$(P)$(R)</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>690</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_2</name>
    <actions>
      <action type="open_display">
        <file>sequencer.bob</file>
        <macros>
          <R>$(R)Wave-</R>
        </macros>
        <target>tab</target>
        <description>Cycling</description>
      </action>
    </actions>
    <x>590</x>
    <y>10</y>
    <rules>
      <rule name="Disable if FASTPS" prop_id="enabled" out_exp="false">
        <exp bool_exp="pvStr0 == &quot;FAST-PS 2020-400&quot;">
          <value>false</value>
        </exp>
        <pv_name>$(P)$(R)Model-R</pv_name>
      </rule>
    </rules>
    <tooltip>$(actions)</tooltip>
  </widget>
</display>
